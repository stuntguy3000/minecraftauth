<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/png" href="../resources/img/favicon.ico">
        
        <meta charset="utf-8">
        <title>MinecraftAuth (Minecraft v1.6.2) Session Login Test</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A way to test the Mojang auth system.">
        <meta name="author" content="">
        
        <style>
        .circular {
			border-radius: 5px;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			box-shadow: 0 0 5px rgba(0, 0, 0, .8);
			-webkit-box-shadow: 0 0 5px rgba(0, 0, 0, .8);
			-moz-box-shadow: 0 0 5px rgba(0, 0, 0, .8);
		}
		</style>
        
        <link href="../resources/css/bootstrap.css" rel="stylesheet" media="screen">
		
        <div id="warningModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="warningModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 id="warningModalLabel">Things you should know about this system</h3>
            </div>
            
            <div class="modal-body">
                <p>This code example/demonstration which is designed to test how the Mojang Auth system works. Think of this as a simulation of the launcher.<br>
                <br> - Do not give out any of the information you receive while using this system. 
                Especially the Token Key. If you release your Token key then it is possible for anyone to login (as you) using that key, 
                as this system is designed to bypass the login section of the new Minecraft launcher.
                <br><br> - This system is experimental and is <strong>not</strong> intended for actual use.
                This is designed for developers to mess around.
                <br><br>You should understand that this system is an experiment, and can be used in dark, scary and pure evil ways.
                </p>
            </div>
            
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
        
	</head>
    
    <body>
		<div class="container">
            <br>
            <center><h2 class="muted">MinecraftAuth (Minecraft v1.6.2) Session Login | Version 1.3.3</h2>
            <a href="http://forums.bukkit.org/threads/resource-bypassing-the-minecraft-launcher.160198/">Click here to visit the thread</a> | <a href="https://bitbucket.org/stuntguy3000/minecraftauth/src/">Source (May not be updated)</a> | Script made by <img class="circular" src="https://minotar.net/avatar/stuntguy3000/16"> <strong>stuntguy3000</strong>.</center>
            <hr>
            <div class="alert alert-info fade in">
                <strong>We respect privacy.</strong> We do not save <strong>any</strong> information that you enter below. This form simply POST's the data to the official <strong>Mojang Authentication servers</strong>, through <strong>Ajax</strong>.
            </div>
            <div id="responce"></div>
            <form class="form-horizontal" id="loginForm">
                <div class="control-group">
                    <label class="control-label">Minecraft Username</label>
                    <div class="controls">
                        <input type="text" id="username" name="username" placeholder="Username" required>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label">Minecraft Password</label>
                    <div class="controls">
                        <input type="password" id="password" name="password" placeholder="Password" required>
                    </div>
                </div>
                
                <div class="control-group" id="tokenGroup" name="tokenGroup">
                    <label class="control-label">Client Token</label>
                    <div class="controls">
                        <input type="text" id="clientToken" name="clientToken" placeholder="Client Token" value="MinecraftIsAwesome">
                    </div>
                </div>
                
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" id="debug" name="debug"> Debug mode?
                        </label>
                    </div>
                </div>
            
                <div class="control-group">
                    <div class="controls">
                        <div class="btn-group">
                            <input type="submit" id="loginButton" class="btn btn-primary" value="Login to Minecraft">
                            <button class="btn btn-info" data-toggle="modal" data-target="#warningModal">Things to know <strong>first</strong></button>
                        </div>
                        
                    </div>
                </div>
            
                <div class="control-group">
                    By submitting this form you agree stuntguy3000 is not responsible for any damages<br> that may (but <strong>should not</strong>) occur to your Mojang/Minecraft account.
                </div>
            </form>-
		</div>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="../resources/js/bootstrap.js"></script>
        
        <script>
            $(document).ready(function(){
                $("#debug").click( function(){
                    if( $(this).is(':checked') ) {
                        $('#tokenGroup').fadeIn();
                    } else {
                        $('#tokenGroup').fadeOut();
                    }
                        
                });
                
                $("#loginForm").submit(function() {
                    $('#loginButton').val('Logging in...');
                    $('#loginButton').addClass('disabled');
                            
                    $('#responce').fadeOut(function() {
                        $.post("post.php", $("#loginForm").serialize(), function(data){
                            $('#responce').html(data);
                            $('#loginButton').val('Login to Minecraft');
                            $('#loginButton').removeClass('disabled');
                            $('#responce').fadeIn();
                        });
                    });
                    return false;
                });
                
                $('#tokenGroup').hide();
                $('#responce').hide();
                $('#userimg').hide();
            });
        </script>
    </body>
</html>
